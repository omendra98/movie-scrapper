# Movie-scrapper web application
This project is related to online movies information. One can view details like 
Genre, Cast, imdb Ratings, Poster, Description etc. of movies in just a single click.
We are using  OMDb API for extracting information from (http://www.omdbapi.com/).

## To run app locally
```bash
- git remote add origin https://gitlab.com/omendra98/movie-scrapper.git
- cd Movie-Scrapper
- git pull origin master
```
- Create your API_KEY from [OMDb API](http://www.omdbapi.com/apikey.aspx)
- Update your API_KEY in ```numb.js```

## Running
After setting up your API_KEY point your browser to movie.html
